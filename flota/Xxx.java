
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


public class Xxx {

    public static void mainxx(String[] args) {
        Integer n = 32;
        List<Integer> nums = Arrays.asList(3, 1, 2, 2, 4, 99, 5, 4, 2);

        TreeMap<Integer, AtomicInteger> numMap = 
                new TreeMap<Integer, AtomicInteger>();

        for (Integer num : nums) {
            AtomicInteger count = numMap.get(num);
            if (count == null) {
                count = new AtomicInteger(0);
                numMap.put(num, count);
            }
            count.incrementAndGet();
        }

        for (Map.Entry<Integer, AtomicInteger> entry : numMap.entrySet()) {
            System.out.print("Ocurrencias de " + entry.getKey() + ": ");
            System.out.println(entry.getValue());
        }

    }


    public static void main(String[] args) {
        Integer n = 32;
        List<Integer> nums = Arrays.asList(3, 1, 2, 2, 4, 99, 5, 4, 2);

        TreeMap<Integer, Integer> numMap = 
                new TreeMap<Integer, Integer>();

        for (Integer num : nums) {
            Integer count = numMap.get(num);
            if (count == null) {
                numMap.put(num, 1);
            } else {
                numMap.put(num, count+1);
            }
        }

        for (Map.Entry<Integer, Integer> entry : numMap.entrySet()) {
            System.out.print("Ocurrencias de " + entry.getKey() + ": ");
            System.out.println(entry.getValue());
        }

    }

}