public abstract class Generico {
    public String execute(int id) {
        String result = null;
        if (id != 0) {
            String value = getValue(id);
            result = doFormat(value);
        } else {
            result = "none";
        }
        return result;
    }
    protected String getValue(int id) {
        String respuesta="Barcelona";
        if (id==17){
            respuesta = "Girona";
        } 
        return respuesta;
    }
    protected abstract String doFormat(String str);
}
