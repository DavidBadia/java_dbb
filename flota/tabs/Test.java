
package tabs;

class Test {

    public static void main(String[] args) {
        
        Producto p = new Producto("paquete de folios");
        Cliente c = new Cliente("Toni Lopez");
        Factura f = new Factura(1234);

        System.out.println( PintadorTabs.pintaTab(p));
        System.out.println( PintadorTabs.pintaTab(c));
        System.out.println( PintadorTabs.pintaTab(f));

        Class x = p.getClass();
        System.out.println(x.getName());
        System.out.println(x.getSimpleName());

    }
}