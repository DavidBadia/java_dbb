<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.enfocat.javaweb.buscaminas.Buscaminas" %>



<%

    if (request.getParameter("reset")!=null){
        Buscaminas.reset(); 
    }

   if (request.getParameter("mostrar")!=null){
        Buscaminas.mostrar(); 
    }

    if (request.getParameter("x")!=null){
        
        int x = Integer.parseInt(request.getParameter("x"));
        int y = Integer.parseInt(request.getParameter("y"));
        Buscaminas.clicar(x,y); 
    }

%>


<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Buscaminas</title>
    <link href="https://fonts.googleapis.com/css?family=New+Rocker&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="css/estilos.css">
</head>

<body>

    <h1>Buscaminas JSP <i class="fas fa-blind"></i><i class="fas fa-bomb"></i></h1>

    <%= Buscaminas.htmlCampo() %>



    <button id="reset">Reset</button>
    <button id="mostrar">Mostrar</button>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>

    <script>

        $(document).on("click", "div.celda.oculta", function () {
            let url = window.location.href.split("?")[0];
            let x = $(this).data("x");
            let y = $(this).data("y");
            window.location.href = url + "?x=" + x + "&y=" + y;
        })

        $(document).on("click", "button#reset", function () {
            let url = window.location.href.split("?")[0];
            window.location.href = url + "?reset=1";
        })

        $(document).on("click", "button#mostrar", function () {
            let url = window.location.href.split("?")[0];
            window.location.href = url + "?mostrar=1";
        })
        $(document).on("click", "div.celda.oculta", function () {
            let url = window.location.href.split("?")[0];
            let x = $(this).data("x");
            let y = $(this).data("y");
            let action = "show";
            window.location.href = url + "?action=" + action + "&x=" + x + "&y=" + y;
        })

        $(document).on("contextmenu", "div.celda.oculta", function (e) {
            if (e.button == 2) {
                e.preventDefault();
                let url = window.location.href.split("?")[0];
                let x = $(this).data("x");
                let y = $(this).data("y");
                let action = "flag";
                window.location.href = url + "?action=" + action + "&x=" + x + "&y=" + y;
            }
        })
    </script>
    <%= Buscaminas.gameOver(Buscaminas.isGameOver())%>

</body>

</html>