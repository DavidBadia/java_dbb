package com.enfocat.javaweb.buscaminas;

public class Mina {
    private static final boolean SHOWHINTS = false;
    private boolean visible;
    private int x;
    private int y;
    private boolean mina = false;
    public String texto;
    private int bombas;

    public Mina(boolean mina, int x, int y, int bombas) {
        this.mina = mina;
        this.x = x;
        this.y = y;
        this.visible = false;
        this.bombas = 0;
        this.texto = "";
    }

    public String html() {
        String css = (this.mina) ? "celda mina " : "celda";

        if (!this.visible)
            css += " oculta";
        if (this.bombas == 0) {
            return String.format("<div class='%s' data-x='%d' data-y='%d'></div>", css, this.x, this.y);
        } else
        // String.format("<div class='%s' data-x='%d' data-y='%d'>%d</div>", css,
        // this.x, this.y, this.bombas);

        if (this.visible) {
            return String.format("<div class='%s' data-x='%d' data-y='%d'>%d</div>", css, this.x, this.y, bombas);
        } else {
            return String.format("<div class='%s' data-x='%d' data-y='%d'></div>", css, this.x, this.y);
        }
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isMina() {
        return mina;
    }

    public void setMina(boolean mina) {
        this.mina = mina;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getBombas() {
        return bombas;
    }

    public void setBombas(int bombas) {
        this.bombas = bombas;
    }

}
