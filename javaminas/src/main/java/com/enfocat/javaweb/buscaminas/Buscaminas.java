
package com.enfocat.javaweb.buscaminas;

import java.util.Random;

public class Buscaminas {
    private static final int ANCHO = 30;
    private static final int ALTO = 10;
    private static final int MINAS = 45;
    private static boolean gameOver = false;
    private static Mina[][] campo = generaCampo(MINAS);
    public int status = 0;

    private static Mina[][] generaCampo(int minas) {
        Mina[][] celdas = new Mina[ALTO][ANCHO];
        for (int y = 0; y < ALTO; y++) {
            for (int x = 0; x < ANCHO; x++) {
                Mina m = new Mina(false, x, y, 0);
                celdas[y][x] = m;
            }
        }
        Random rnd = new Random();
        for (int m = 0; m < minas; m++) {
            int rndX = rnd.nextInt(ANCHO);
            int rndY = rnd.nextInt(ALTO);
            celdas[rndY][rndX].setMina(true);
        }
        for (int y = 0; y < ALTO; y++) {
            for (int x = 0; x < ANCHO; x++) {
                int minasCercanas = Buscaminas.buscaBombas(x, y, celdas);
                Mina m = celdas[y][x];
                m.setBombas(minasCercanas);
            }
        }
        return celdas;
    }

    public static Mina[][] getCampo() {
        return Buscaminas.campo;
    }

    public static void reset() {
        Buscaminas.campo = generaCampo(MINAS);
        Buscaminas.setGameOver(false);
    }

    public static void mostrar() {
        for (int y = 0; y < ALTO; y++) {
            for (int x = 0; x < ANCHO; x++) {
                Buscaminas.campo[y][x].setVisible(true);
            }
        }
    }

    public static String gameOver(boolean GameOver) {
        String game = "";
        if (gameOver == true) {
            game = "<div class='over'></div>";
        }
        return String.format(game);

    }

    public static void clicar(int x, int y) {
        Mina m = Buscaminas.campo[y][x];
        m.setVisible(true);
        m.setBombas(Buscaminas.busca(x, y));
        if (m.getBombas() == 0 && !m.isMina()) {
            alrededor(x, y);
        }
        Buscaminas.campo[y][x].setVisible(true);
        if (Buscaminas.campo[y][x].isMina() == true) {
            Buscaminas.mostrar();
            Buscaminas.setGameOver(true);
        }

    }

    public static void alrededor(int x, int y) {
        Buscaminas.revisa(x - 1, y - 1);
        Buscaminas.revisa(x, y - 1);
        Buscaminas.revisa(x + 1, y - 1);
        Buscaminas.revisa(x - 1, y);
        Buscaminas.revisa(x + 1, y);
        Buscaminas.revisa(x - 1, y + 1);
        Buscaminas.revisa(x, y + 1);
        Buscaminas.revisa(x + 1, y + 1);
    }

    public static int minaCercana(int x, int y) {
        int numeroMinas = Buscaminas.busca(x - 1, y - 1) + Buscaminas.busca(x, y - 1) + Buscaminas.busca(x + 1, y - 1)
                + Buscaminas.busca(x - 1, y) + Buscaminas.busca(x + 1, y) + Buscaminas.busca(x - 1, y + 1)
                + Buscaminas.busca(x, y + 1) + Buscaminas.busca(x + 1, y + 1);
        return numeroMinas;
    }

    public static int busca(int x, int y) {
        if (x >= ANCHO || x < 0 || y < 0 || y >= ALTO) {
            return 0;
        }
        if (Buscaminas.campo[y][x].isMina() == true) {
            return 1;
        } else {
            return 0;
        }
    }

    public static void revisa(int x, int y) {

        if (x >= ANCHO || x < 0 || y < 0 || y >= ALTO) {
            return;
        }
        Mina pos = Buscaminas.campo[y][x];

        if (pos.isMina() || pos.isVisible()) {
            return;
        }

        if (pos.getBombas() > 0) {
            pos.setVisible(true);
        }

        if (pos.getBombas() == 0) {
            Buscaminas.clicar(x, y);
        }

    }

    public static boolean noEsBomba(int x, int y) {
        return false;
    }

    private static int esMina(int x, int y, Mina[][] campo) {
        if (x >= ANCHO || x < 0 || y < 0 || y >= ALTO) {
            return 0;
        }
        Mina m = campo[y][x];
        return m.isMina() ? 1 : 0;
    }

    public static int buscaBombas(int x, int y) {

        Mina[][] celdas = Buscaminas.campo;
        return Buscaminas.esMina(x - 1, y - 1, celdas) + Buscaminas.esMina(x, y - 1, celdas)
                + Buscaminas.esMina(x + 1, y - 1, celdas) + Buscaminas.esMina(x - 1, y, celdas)
                + Buscaminas.esMina(x + 1, y, celdas) + Buscaminas.esMina(x - 1, y + 1, celdas)
                + Buscaminas.esMina(x, y + 1, celdas) + Buscaminas.esMina(x + 1, y + 1, celdas);
    }

    public static int buscaBombas(int x, int y, Mina[][] celdas) {

        return Buscaminas.esMina(x - 1, y - 1, celdas) + Buscaminas.esMina(x, y - 1, celdas)
                + Buscaminas.esMina(x + 1, y - 1, celdas) + Buscaminas.esMina(x - 1, y, celdas)
                + Buscaminas.esMina(x + 1, y, celdas) + Buscaminas.esMina(x - 1, y + 1, celdas)
                + Buscaminas.esMina(x, y + 1, celdas) + Buscaminas.esMina(x + 1, y + 1, celdas);

    }

    public static String htmlCampo() {

        StringBuilder sb = new StringBuilder();
        String iniDivFila = "<div class='fila'>";
        String finDiv = "</div>";

        sb.append("<div class='campo'>");
        for (int y = 0; y < ALTO; y++) {
            sb.append(iniDivFila);
            for (int x = 0; x < ANCHO; x++) {
                sb.append(Buscaminas.campo[y][x].html());
            }
            sb.append(finDiv);
        }
        sb.append(finDiv);

        return sb.toString();

    }

    public static boolean isGameOver() {
        return gameOver;
    }

    public static void setGameOver(boolean gameOver) {
        Buscaminas.gameOver = gameOver;
    }

}