<%@page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  




        <h3>Servlet + JSP + tags</h3>
        <table class="table">
            <c:forEach items="${contactos}" var="contacto">
                <tr>
                    <td>${contacto.id}</td>
                    <td><c:out value="${contacto.nombre}" /></td>
                    <td><c:out value="${contacto.email}" /></td>
                    <td><c:out value="${contacto.ciudad}" /></td>
                    <td><c:out value="${contacto.telefono}" /></td>
                </tr>
            </c:forEach>
        </table>
