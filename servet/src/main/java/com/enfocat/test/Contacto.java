package com.enfocat.test;

public class Contacto {

    private int id;
    private String nombre;
    private String email;
    private String ciudad;
    private String telefono;
    private String urlfoto;
    private String piefoto;

    // CONSTRUCTOR SIN ID
    public Contacto(String nombre, String email, String ciudad, String telefono) {
        this.id = 0;
        this.nombre = nombre;
        this.email = email;
        this.ciudad = ciudad;
        this.telefono = telefono;
    }

    // CONSTRUCTOR CON ID
    public Contacto(int id, String nombre, String email, String ciudad, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.ciudad = ciudad;
        this.telefono = telefono;
    }

    public Contacto(int id, String nombre, String email, String ciudad, String telefono, String urlfoto,
            String piefoto) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.ciudad = ciudad;
        this.telefono = telefono;
        this.urlfoto = urlfoto;
        this.piefoto = piefoto;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", this.nombre, this.email, this.ciudad, this.telefono);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUrlfoto() {
        return this.urlfoto;
    }

    public String getUrlfotoPath() {
        String base = "/contactos/uploads/";
        if (this.urlfoto == null || this.urlfoto.length() == 0) {
            return base + "nofoto.png";
        } else {
            return base + this.urlfoto;
        }
    }

    protected void setUrlfoto(String urlfoto) {
        this.urlfoto = urlfoto;
    }

    public String getPiefoto() {
        return piefoto;
    }

    public void setPiefoto(String piefoto) {
        this.piefoto = piefoto;
    }

}