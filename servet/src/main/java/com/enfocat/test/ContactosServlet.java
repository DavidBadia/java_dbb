package com.enfocat.test;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enfocat.test.DBDatos;
import com.enfocat.test.Contacto;

public class ContactosServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Contacto> products = DBDatos.getContactos();

        request.setAttribute("contactos", products);
        request.getRequestDispatcher("/contactos.jsp").forward(request, response);
    }

}
