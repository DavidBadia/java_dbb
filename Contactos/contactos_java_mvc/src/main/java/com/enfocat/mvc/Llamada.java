package com.enfocat.mvc;

import java.util.Date;

public class Llamada {

    private int id;
    private String fecha;
    private String nota;
    private int contactos_id;

    public Llamada(String fecha, String nota, int contactos_id) {
        this.id = 0;
        this.fecha = fecha;
        this.nota = nota;
        this.contactos_id = contactos_id;
    }

    // CONSTRUCTOR CON ID

    public Llamada(int id, String fecha, String nota, int contactos_id) {
        this.id = id;
        this.fecha = fecha;
        this.nota = nota;
        this.contactos_id = contactos_id;
    }

    public Llamada(int id, Date fecha, String nota, int contactos_id) {
        this.id = id;
        this.fecha = "xxxx";
        this.nota = nota;
        this.contactos_id = contactos_id;
    }

    @Override
    public String toString() {
        return String.format("%s %s", this.fecha, this.nota, this.contactos_id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public int getContactos_id() {
        return contactos_id;
    }

    public void setContactos_id(int contactos_id) {
        this.contactos_id = contactos_id;
    }

}
