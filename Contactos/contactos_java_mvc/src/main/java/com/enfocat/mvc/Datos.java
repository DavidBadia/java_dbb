package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;

public class Datos {

    private static List<Contacto> contactos = new ArrayList<Contacto>();
    private static int ultimoContacto = 0;

    static {
        contactos.add(new Contacto(1, "ana", "ana@gmail.com", "Badalona", "754839219", "avatar1.jpg", "foto1"));
        contactos.add(new Contacto(2, "joan", "joan@gmail.com", "Manresa", "65743829", "avatar2.jpg", "foto2"));
        ultimoContacto = 2;
    }

    public static Contacto newContacto(Contacto cn) {
        ultimoContacto++;
        cn.setId(ultimoContacto);
        contactos.add(cn);
        return cn;
    }

    public static Contacto getContactoId(int id) {
        for (Contacto cn : contactos) {
            if (cn.getId() == id) {
                return cn;
            }
        }
        return null;
    }

    public static boolean updateContacto(Contacto cn) {
        boolean updated = false;
        for (Contacto x : contactos) {
            if (cn.getId() == x.getId()) {
                // x = cn;
                int idx = contactos.indexOf(x);
                contactos.set(idx, cn);
                updated = true;
                break;
            }
        }
        return updated;
    }

    public static boolean deleteContactoId(int id) {
        boolean deleted = false;

        for (Contacto x : contactos) {
            if (x.getId() == id) {
                contactos.remove(x);
                deleted = true;
                break;
            }
        }
        return deleted;
    }

    public static List<Contacto> getContactos() {
        return contactos;
    }

}
