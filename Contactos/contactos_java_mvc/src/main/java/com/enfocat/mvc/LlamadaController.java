package com.enfocat.mvc;

import java.util.List;

public class LlamadaController {
    static final String MODO = "db";

    public static List<Llamada> getAll() {
        if (MODO.equals("db")) {
            return DBDatos.getLlamadas();
        } else {
            return DatosLlamada.getLlamadas();
        }
    }

    public static Llamada getLlamadaById(int id) {
        if (MODO.equals("db")) {
            return DBDatos.getLlamadaId(id);
        } else {
            return DatosLlamada.getLlamadaId(id);
        }
    }

    public static void save(Llamada ll) {
        if (MODO.equals("db")) {
            if (ll.getId() > 0) {
                DBDatos.updateLlamada(ll);
            } else {
                DBDatos.newLlamada(ll);
            }
        } else {
            if (ll.getId() > 0) {
                DatosLlamada.updateLlamada(ll);
            } else {
                DatosLlamada.newLlamada(ll);
            }
        }

    }

    public static int size() {
        if (MODO.equals("db")) {
            return DBDatos.getLlamadas().size();
        } else {
            return DatosLlamada.getLlamadas().size();
        }
    }

    public static void removeId(int id) {
        if (MODO.equals("db")) {
            DBDatos.deleteLlamadaId(id);
        } else {
            DatosLlamada.deleteLlamadaId(id);
        }
    }

}