package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;

public class DatosLlamada {

    private static List<Llamada> Llamadas = new ArrayList<Llamada>();
    private static int ultimoLlamada = 0;

    /*
     * static { Llamadas.add(new Llamada(1, "2-11-2019", "Llama a Juan", 3));
     * Llamadas.add(new Llamada(2, "3-5-2018", "Compra Pan", 3)); ultimoLlamada = 2;
     * }
     */

    public static Llamada newLlamada(Llamada ll) {
        ultimoLlamada++;
        ll.setId(ultimoLlamada);
        Llamadas.add(ll);
        return ll;
    }

    public static Llamada getLlamadaId(int id) {
        for (Llamada ll : Llamadas) {
            if (ll.getId() == id) {
                return ll;
            }
        }
        return null;
    }

    public static boolean updateLlamada(Llamada ll) {
        boolean updated = false;
        for (Llamada x : Llamadas) {
            if (ll.getId() == x.getId()) {
                // x = ll;
                int idx = Llamadas.indexOf(x);
                Llamadas.set(idx, ll);
                updated = true;
            }
        }
        return updated;
    }

    public static boolean deleteLlamadaId(int id) {
        boolean deleted = false;

        for (Llamada x : Llamadas) {
            if (x.getId() == id) {
                Llamadas.remove(x);
                deleted = true;
                break;
            }
        }
        return deleted;
    }

    public static List<Llamada> getLlamadas() {
        return Llamadas;
    }

}
