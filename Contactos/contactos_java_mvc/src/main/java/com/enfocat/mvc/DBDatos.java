package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import com.mysql.jdbc.Connection;

public class DBDatos {

    private static final String TABLE = "contactos";
    private static final String TABLE1 = "llamadas";
    private static final String KEY = "id";

    public static Contacto newContacto(Contacto cn) {
        String sql;

        sql = String.format("INSERT INTO %s (nombre, email, ciudad, telefono, urlfoto, piefoto) VALUES (?,?,?,?,?,?)",
                TABLE);

        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {

            pstmt.setString(1, cn.getNombre());
            pstmt.setString(2, cn.getEmail());
            pstmt.setString(3, cn.getCiudad());
            pstmt.setString(4, cn.getTelefono());
            pstmt.setString(5, cn.getUrlfoto());
            pstmt.setString(6, cn.getPiefoto());
            pstmt.executeUpdate();

            // usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
            if (rs.next()) {
                cn.setId(rs.getInt(1));
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return cn;

    }

    public static Contacto getContactoId(int id) {
        Contacto cn = null;
        String sql = String.format("select %s, nombre, email, ciudad, telefono, urlfoto, piefoto from %s where %s=%d",
                KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                cn = new Contacto((Integer) rs.getObject(1), (String) rs.getObject(2), (String) rs.getObject(3),
                        (String) rs.getObject(4), (String) rs.getObject(5), (String) rs.getObject(6),
                        (String) rs.getObject(7));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return cn;
    }

    public static boolean updateContacto(Contacto cn) {
        boolean updated = false;
        String sql;

        sql = String.format("UPDATE %s set nombre=?, email=?, ciudad=?, telefono=?, urlfoto=?, piefoto=? where %s=%d",
                TABLE, KEY, cn.getId());

        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {

            pstmt.setString(1, cn.getNombre());
            pstmt.setString(2, cn.getEmail());
            pstmt.setString(3, cn.getCiudad());
            pstmt.setString(4, cn.getTelefono());
            pstmt.setString(5, cn.getUrlfoto());
            pstmt.setString(6, cn.getPiefoto());
            pstmt.executeUpdate();
            updated = true;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return updated;
    }

    public static boolean deleteContactoId(int id) {
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            deleted = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return deleted;
    }

    public static List<Contacto> getContactos() {

        List<Contacto> listaContactos = new ArrayList<Contacto>();

        String sql = String.format("select id,nombre,email,ciudad,telefono, urlfoto, piefoto from contactos");

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Contacto u = new Contacto((Integer) rs.getObject(1), (String) rs.getObject(2), (String) rs.getObject(3),
                        (String) rs.getObject(4), (String) rs.getObject(5), (String) rs.getObject(6),
                        (String) rs.getObject(7));
                listaContactos.add(u);
            }

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaContactos;
    }

    public static Llamada newLlamada(Llamada ll) {
        String sql;

        sql = String.format("INSERT INTO %s (fecha, nota, contactos_id) VALUES (?,?,?)", TABLE1);

        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, ll.getFecha());
            pstmt.setString(2, ll.getNota());
            pstmt.setInt(3, ll.getContactos_id());
            pstmt.executeUpdate();

            ResultSet rs = stmt.executeQuery("select last_insert_id()");
            if (rs.next()) {
                ll.setId(rs.getInt(1));
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return ll;

    }

    public static Llamada getLlamadaId(int id) {
        Llamada ll = null;
        String sql = String.format("select %s, fecha, nota, contactos_id from %s where %s=%d", KEY, TABLE1, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                ll = new Llamada((Integer) rs.getObject(1), (Date) rs.getObject(2), (String) rs.getObject(3),
                        (Integer) rs.getObject(4));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return ll;
    }

    public static boolean updateLlamada(Llamada ll) {
        boolean updated = false;
        String sql;

        sql = String.format("UPDATE %s set fecha=?, nota=?, contactos_id=? where %s=%d", TABLE1, KEY, ll.getId());

        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, ll.getFecha());
            pstmt.setString(2, ll.getNota());
            pstmt.setInt(3, ll.getContactos_id());
            pstmt.executeUpdate();

            updated = true;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return updated;
    }

    public static boolean deleteLlamadaId(int id) {
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE1, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            deleted = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return deleted;
    }

    public static List<Llamada> getLlamadas() {

        List<Llamada> listaLlamadas = new ArrayList<Llamada>();

        String sql = "select id,fecha,nota,contactos_id from llamadas";

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Llamada u = new Llamada((Integer) rs.getObject(1), (Date) rs.getObject(2), (String) rs.getObject(3),
                        (Integer) rs.getObject(4));

                listaLlamadas.add(u);
            }

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaLlamadas;
    }

}
