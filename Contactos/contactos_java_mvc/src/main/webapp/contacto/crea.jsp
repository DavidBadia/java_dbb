<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>
<%@page import="java.util.*" %>


<%
    boolean datosOk;
 DBDatos datos = new DBDatos();

    if ("POST".equalsIgnoreCase(request.getMethod())) {
   
        request.setCharacterEncoding("UTF-8");

        String nombre = request.getParameter("nombre");
        String email = request.getParameter("email");
        String ciudad = request.getParameter("ciudad");
        String telefono = request.getParameter("telefono");

       
        datosOk=true;
        if (datosOk){
            Contacto a = new Contacto(nombre, email, ciudad, telefono);
            ContactoController.save(a);
            response.sendRedirect("/contactos/contacto/list.jsp");
            return;
        }
    }


%>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>

<%@include file="/menu.jsp"%>

<div class="container">
<div class="row">
<div class="col">
<h1>Nuevo contacto</h1>
</div>
</div>


<div class="row">
<div class="col-md-8">

<form id="formcrea" action="#" method="POST">
        <div class="form-group">
            <label for="nombreInput">Nombre del contacto</label>
            <input  name="nombre"  type="text" class="form-control" id="nombreInput" placeholder="Introducir Nombre">
        </div>
        <div class="form-group">
            <label for="emailInput">Email del contacto</label>
            <input  name="email"  type="text" class="form-control" id="emailInput" placeholder="Introducir Email">
        </div>
        <div class="form-group">
            <label for="ciudadInput">Ciudad del contacto</label>
            <input  name="ciudad"  type="text" class="form-control" id="ciudadInput"  placeholder="Introducir Ciudad">
        </div>
        <div class="form-group">
            <label for="telefonoInput">Telefono del contacto</label>
            <input  name="telefono"  type="number" class="form-control" id="telefonoInput"  placeholder="Introducir Telefono">
        </div>
        
 <button type="submit" class="btn btn-primary">Guardar</button>
           
</form>


</div>
</div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

</body>
</html>
