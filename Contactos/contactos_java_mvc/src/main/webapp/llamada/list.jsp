<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>
<%@page import="java.text.*, java.util.Date" %>

<%
System.out.println("Estem on hem d'estar");
ContactoController contacto = new ContactoController();

%>
<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ContactosApp</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>


<%@include file="/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">
<h1>Listado de Llamadas <i class="fas fa-phone"></i></h1>
</div>
</div>

<div class="row">
<div class="col">

<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Fecha</th>
      <th scope="col">Nota</th>
      <th scope="col">Editar</th>
      <th scope="col">Eliminar</th>
   
    
    </tr>
  </thead>
  <tbody>

   <% for (Llamada ll : LlamadaController.getAll()) { %>
        <tr>
        <th scope="row"><%= ll.getId() %></th>
       <%
       int nombre = ll.getContactos_id();    
        %>
      <td><%= contacto.getContactById(nombre) %></td>
        <td><%= ll.getFecha() %></td>
        <td><%= ll.getNota() %></td>
        
        <td><a href="/contactos/llamada/edita.jsp?id=<%= ll.getId() %>"><button type="button" class="btn btn-primary"><i class="far fa-edit fa-2x"></i></button></a></td>
        <td><a href="/contactos/llamada/elimina.jsp?id=<%= ll.getId() %>"><button type="button" class="btn btn-danger"><i class="fas fa-trash-alt fa-2x"></i></button></a></td>
      </tr>
    <% } %>
  </tbody>
</table>

<a href="/contactos/llamada/crea.jsp" class="btn btn-warning btn-lg"><i class="fas fa-file-signature fa-2x"></i></a>
</div>
</div>

</div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/contactos/js/scripts.js"></script>
</body>
</html>
