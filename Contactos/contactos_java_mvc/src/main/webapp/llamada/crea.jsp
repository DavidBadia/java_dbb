<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>
<%@page import="java.text.*" %>
<%@page import="java.util.Date" %>
<%
    boolean datosOk;
    DBDatos datos = new DBDatos();

    if ("POST".equalsIgnoreCase(request.getMethod())) {
     
        request.setCharacterEncoding("UTF-8");
      
        String fecha = request.getParameter("fecha");
        String nota = request.getParameter("nota");
        int contactos_id = Integer.parseInt(request.getParameter("contactos"));

      DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date formatoFecha = sdf.parse(fecha);
            System.out.println(formatoFecha);
            fecha = sdf.format(formatoFecha);
          System.out.println(fecha);

        
        int idContacto = 0;

        for (int y = 0; y < datos.getContactos().size(); y++) {           
          if( datos.getContactos().get(y).getNombre().equals(contactos_id)) {
            idContacto = datos.getContactos().get(y).getId();
          }
        }

        datosOk=true;
        if (datosOk){
            Llamada ll = new Llamada(fecha, nota, contactos_id);
            LlamadaController.save(ll);
            response.sendRedirect("/contactos/contacto/list.jsp");
            return;
        }
    }
String contactos = " ";

    for (int i = 0; i < datos.getContactos().size(); i++) {           
     
     contactos = contactos + String.format("<option>"+datos.getContactos().get(i).getNombre()+"</option>");
     }

%>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>

<%@include file="/menu.jsp"%>

<div class="container">
<div class="row">
<div class="col">
<h1>Nueva Llamada</h1>
</div>
</div>


<div class="row">
<div class="col-md-8">

<div class="form-group">
    <label for="contactoInput">Contacto</label>
    <select  name="contacto"  type="text" class="form-control" id="contactoInput">
    <%= contactos %>
    </select>
  </div>
  <div class="form-group">
    <label for="fechaInput">Fecha de la Llamada</label>
    <input  name="fecha"  type="text" class="form-control" id="fechaInput" placeholder="yyyy-MM-dd">
  </div>
  
 <div class="form-group">
    <label for="notaInput">Nota</label>
    <input  name="nota"  type="text" class="form-control" id="notaInput"  placeholder="Deja tu nota.">
  </div>

 
  
    <button type="submit" class="btn btn-primary">Guardar</button>
    
</form>


</div>
</div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

</body>
</html>
