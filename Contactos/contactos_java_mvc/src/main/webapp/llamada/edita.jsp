<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>
<%@page import="java.text.*" %>
<%@page import="java.util.Date" %>

<%

    Llamada ll = null;
    boolean datos_guardados=false;

  
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");
    if (id==null){
        response.sendRedirect("/llamada");
         return;
    }else{
       
        if ("POST".equalsIgnoreCase(request.getMethod())) {
           
                
                int id_numerico = Integer.parseInt(id);
                String fecha = request.getParameter("fecha");
                String nota = request.getParameter("nota");
                String contactos_id = request.getParameter("contactos_id");
             
               
                ll = new Llamada(id_numerico,fecha,nota,contactos_id);
                LlamadaController.save(ll);
                datos_guardados=true;
              
                response.sendRedirect("/contactos/llamada/list.jsp");
                return;
        } else {
            
            ll = LlamadaController.getId(Integer.parseInt(id));
            if (ll==null) {
                    response.sendRedirect("/llamada");
                    return;
            } 
        }
    }
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>

<%@include file="/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">
<h1>Editar Llamada para <%= ll.getContactos_id() %></h1>
</div>
</div>

<div class="row">
<div class="col-md-8">

<form id="formcrea" action="#" method="POST">
  <div class="form-group">
    <label for="fechaInput">Fecha de la Llamada</label>
    <input  name="fecha"  type="text" class="form-control" id="fechaInput" value="<%= ll.getFecha() %>">
  </div>
  
 <div class="form-group">
    <label for="notaInput">Nota</label>
    <input  name="nota"  type="text" class="form-control" id="notaInput" value="<%= ll.getNota() %>" placeholder="Deja tu nota.">
  </div>

  
  <!-- guardamos id en campo oculto! -->
    <input type="hidden" name="id" value="<%= ll.getId() %>">
    <button type="submit" class="btn btn-primary">Guardar</button>
    
</form>
<br>
<br>
<br>
<% if (datos_guardados==true) { %>

<h1 class="rojo">Datos guardados!</h1>
<%}%>

</div>
</div>

</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
 


</body>
</html>
